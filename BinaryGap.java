﻿public class BinaryGap {
    
    private static int solution(int n) {
        int result = 0;
        int counter = 0;
        char [] tab = Integer.toBinaryString(n).toCharArray();
        for(int i=0; i<tab.length; i++) {
            if(tab[i]=='0') {
                counter++;
            } else {
                if(counter>result) {
                    result = counter;
                }
                counter = 0;
            }
        }
        return result;
    }
    
    public static void main(String[] args) {
        int x = 74901729;
        System.out.println(solution(x));
    }
}