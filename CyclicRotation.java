﻿public class CyclicRotation {
    
    private static int[] solution(int[] A, int K) {
        if(A.length == 0) {
            return A;
        }
        for(int i=0; i<K; i++) {
            int temp = A[A.length-1];
            for(int j=A.length-1; j>0; j--) {
                A[j] = A[j-1];
            }
            A[0] = temp;
        }
        return A;
    }
    
    public static void main(String[] args) {
        int[] a = {3,8,9,7,6};
        int k = 3;
        for (int x : solution(a,k)) {
            System.out.println(x);
        }
    }
}