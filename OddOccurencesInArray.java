﻿public class OddOccurencesInArray {
    
    private static int solution(int[] A) {
        int result = 0;
        for (int i=0; i<A.length; i++) {
            result = result ^ A[i];
        }
        return result;
    }
    
    public static void main(String[] args) {
        int[] a = {9,3,9,3,9,7,9};
        System.out.println(solution(a));
    }
}